from django.shortcuts import render, redirect
from .models import Todo
from . import forms


# Create your views here.
def home(request):
    return render(request, 'home.html')


def todo(request):
    todos = Todo.objects.all().order_by('date')
    return render(request, 'todo.html', {'todos': todos})

def todo_create(request):
    if request.method == 'POST':
        form = forms.TodoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('todo')

    else:
        form = forms.TodoForm()
    return render(request, 'todo_create.html', {'form': form})


def todo_delete(request):
	Todo.objects.all().delete()
	return render(request, "todo.html")

def todo_deletesatu(request, id):
    todo = Todo.objects.get(id=id)
    todo.delete()
    return redirect('todo')

